#! /usr/bin/bash

#SBATCH -J gstacks

## Un argument nécessaire, un directory. 
## Le directory doit finir par un "/":  data/

directory=$1

outfile=sortie_${directory//\//_}

rm -rf $outfile

bash scripts/bonjour.sh >> $outfile


echo -e "\nSortie produite le:" >> $outfile
date >> $outfile

echo -e "\nVoici l'arborescence à partir de $directory"  >> $outfile

tree -L 2 >> $outfile

echo -e "\nVoici les types de fichiers de $directory" >> $outfile

file ${directory}* >> $outfile

echo -e "\nVoici la taille du contenu de $directory" >> $outfile

du -sh ${directory}* >> $outfile
